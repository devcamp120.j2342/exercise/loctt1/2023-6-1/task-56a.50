package com.devcamp.s50.task56a50.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class InvoicesController {
     @GetMapping("/invoices")
     public ArrayList<InvoiceItem> getInvoiceItems(){
          InvoiceItem item1 = new InvoiceItem("1", "ga", 20, 2.5);
          InvoiceItem item2 = new InvoiceItem("2", "vit", 30, 3.5);
          InvoiceItem item3 = new InvoiceItem("3", "cho", 40, 4.5);

          System.out.println(item1.toString());
          System.out.println(item2.toString());
          System.out.println(item3.toString());

          ArrayList<InvoiceItem> invoices = new ArrayList<>();
          invoices.add(item1);
          invoices.add(item2);
          invoices.add(item3);

          return invoices;
     }
}
